from setuptools import setup, find_packages

name = 'generalutils'

setup(
    name=name,
    version="0.1",
    author='timjolson',
    author_email='3058949-timjolson@users.noreply.gitlab.com',
    packages=find_packages(),
    tests_require=['pytest'],
)
